# pip install pafy
# pip install youtube-dl
import pafy
import cv2

url = "https://www.youtube.com/watch?v=Qso8mefKH34"
url = "https://www.youtube.com/watch?v=wdFFZ8tcPXY"
url = "https://www.youtube.com/watch?v=wdFFZ8tcPXY"
url = "https://www.youtube.com/watch?v=mXkC_42jbPU"
output = "downloaded.avi"
video = pafy.new(url)
#stream = video.getbest()
stream = video.streams[0]

print(stream)


capture = cv2.VideoCapture(stream.url)
capture.set(cv2.CAP_PROP_BUFFERSIZE, 2)
capture.set(cv2.CAP_PROP_POS_MSEC, 5000)


width, height = stream.dimensions

fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
out = cv2.VideoWriter(output,fourcc, 30, (int(width),int(height)))

while True:
    if capture.isOpened():
        (status, frame) = capture.read()
        cv2.imshow('frame', frame)
        out.write(frame)
        if cv2.waitKey(1) == ord('q'):
            break

capture.release()
out.release()
cv2.destroyAllWindows()
exit(1)


