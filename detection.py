import cv2
import torch
from yolov5 import YOLOv5
from shapely.geometry.polygon import Polygon
import numpy as np

# Change here:
restricted_area = [(264, 271), (556, 381), (520, 445), (199, 418), (188, 270)]
video_link = 'dog_sofa.mp4'
#video_link = 'downloaded2.avi'

# Don't change:
restricted_poly = Polygon(restricted_area)

# Threshold and restricted zone area
area = restricted_poly.area
threshold = 0.3

model_path = "yolo/weights/generated_2021_11_14.pt"
device = 'cuda' if torch.cuda.is_available() else 'cpu'
yolov5 = YOLOv5(model_path, device)

cap = cv2.VideoCapture(video_link)
ret, frame = cap.read()
assert ret, "Can't read video"

while(cap.isOpened()):
    ret, frame = cap.read()
    if not ret:
        break # video end
    
    results = yolov5.predict(frame, size=640)

    # Check if any pets have intersection with the restricted area
    alarm = False
    for x0, y0, x1, y1, confidence, cl in results.xyxy[0]:
        bbox = Polygon([(x0, y0), (x0, y1), (x1, y1), (x1, y0)])
        intersection = restricted_poly.intersection(bbox)

        if (intersection.area / bbox.area) > threshold:
            alarm = True

    # Draw restricted area:
    cv2.polylines(frame, [np.int32(restricted_area)], True, (0, 0, 255), 3)
    
    # Draw bounding boxes for pets:
    results.display(render=True)

    if alarm:
        cv2.putText(frame, "ALARM!", (10,60), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 5, cv2.LINE_AA)

    cv2.imshow('frame', frame)

    if cv2.waitKey(100) & 0xFF == ord('q'): # Press Q on keyboard to exit
        break

cap.release()
cv2.destroyAllWindows()
