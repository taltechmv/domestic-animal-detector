import cv2
import torch

# set model params
model = torch.hub.load('ultralytics/yolov5', 'yolov5s')
model.conf = 0.1    # Confidence interval
model.classes = 15  # Select classes for detection (15 := cats; 16 := dogs)

cap = cv2.VideoCapture('downloaded2.avi')

if not cap.isOpened():
    print("Error opening video stream or file")

# Read until video is completed
while cap.isOpened():
    # Capture frame-by-frame
    ret, frame = cap.read()
    if not ret:
        break

    results = model(frame)

    results.display(render=True)
    cv2.imshow('frame', results.imgs[0])

    # Press Q on keyboard to  exit
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


cap.release()
cv2.destroyAllWindows()
