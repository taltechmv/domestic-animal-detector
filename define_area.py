import cv2  
import numpy as np 

cap = cv2.VideoCapture('downloaded2.avi')

if (cap.isOpened()== False): 
  print("Error opening video stream or file")

# Read until video is completed
assert(cap.isOpened())
ret, img = cap.read()
assert(ret)


points = []

# mouse callback function
def line_drawing(event,x,y,flags,param):
    global points, img
  
    if event==cv2.EVENT_LBUTTONUP:
        points.append((x, y))
    elif event==cv2.EVENT_RBUTTONUP:
        print("Here's your polygon: %s" % points)
        points = []
    else:
        return

    # redraw everything
    ret, img = cap.read()
    assert(ret)
    for i in range(len(points)):
        color = (255,255,255)
        if 0 == i:
            color = (0, 255, 0)
        cv2.line(img, points[i-1], points[i], color=color, thickness=2)

cv2.namedWindow('draw window')
cv2.setMouseCallback('draw window', line_drawing)

while(1):
    cv2.imshow('draw window',img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()