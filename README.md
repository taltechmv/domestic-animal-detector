**Preparation**
- Install Microsoft C++ build tools using [this](https://visualstudio.microsoft.com/visual-cpp-build-tools/ )
- Install torch with CUDA support: 

    `pip3 install torch==1.10.0+cu102 torchvision==0.11.1+cu102 torchaudio===0.10.0+cu102 -f https://download.pytorch.org/whl/cu102/torch_stable.html`

- Install yolo v5: `pip install yolov5`
- Install NVidia [CUDA Toolkit](https://developer.nvidia.com/cuda-downloads )
- Install shapely: `pip install shapely`

**Defining restricted area**

Run script `define_area.py`. Add points with left mouse click. When you are done, right click. It will print out the polygon coordinates that can be copied to file detection.py

**Using the detector**

Run script `detector.py`. Change the `video_link` and `restricted_area` variables as needed. `restricted_area` can be generated with script `define_area.py`.

**Generating own model detecting only cats and dogs**

This is how the model was generated.
- Downloaded images from: https://www.kaggle.com/tarunbisht11/yolo-animal-detection-small
- Downloaded script `scripts/convert.py` from [here](https://michaelohanu.medium.com/yolov5-tutorial-75207a19a3aa) and converted the .xml annotations to yolo .txt format 
- Manually copied resulting .jpg and .txt files to correct subdirectiories under `images/`
- Ran training (paths are local to my computer):

```
F:
cd \Dropbox\Documents\kool\MachineVision\OpenCV\domestic_animal
python C:\users\user\appdata\local\packages\pythonsoftwarefoundation.python.3.9_qbz5n2kfra8p0\localcache\local-packages\python39\site-packages\yolov5\train.py --img 640 --batch 8 --epochs 200 --data .\data\catsdogs.yaml --cfg .\data\custom_yolov5s.yaml --weights .\yolov5s.pt
```


- Copied the resulting weights file: 

    `cp runs/train/exp23/weights/best.pt .\yolov5\weights\generated_2021_10_31.pt`
