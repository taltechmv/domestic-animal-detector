import cv2
import torch
from yolov5 import YOLOv5
import numpy as np
from lib.snapshot import snapshot
import math

video_link = 'dog_sofa.mp4'
#video_link = 'downloaded2.avi'
#video_link = 'downloaded.avi'
model_path = "yolo/weights/generated_2021_11_14.pt"

device = 'cuda' if torch.cuda.is_available() else 'cpu'
yolov5 = YOLOv5(model_path, device)

cap = cv2.VideoCapture(video_link)
ret, frame = cap.read()
assert ret, "Can't read video"

frame_no = 0
frame_count = cap.get(cv2.CAP_PROP_FRAME_COUNT)

while(cap.isOpened()):
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_no)
    ret, frame = cap.read()
    if not ret:
        break # video end

    orig_frame = frame.copy()
    
    results = yolov5.predict(frame, size=640)

    # Draw bounding boxes for pets:
    results.display(render=True)

    # Draw progress bar:
    y, w, _ = frame.shape
    y -= 20
    complete = frame_no / frame_count
    cv2.line(frame, (0, y), (w, y), (255,255,255), 2)
    cv2.line(frame, (0, y), (math.ceil(w*complete), y), (0,0,255), 2)

    cv2.imshow('frame', frame)

    key = cv2.waitKeyEx(0)
    if key == ord('s'):
        snapshot(orig_frame, video_link)
    elif key == 2555904: # Arrow right. Seek 1 frame forward
        frame_no = max(0, frame_no + 1)
    elif key == 2424832: # Arrow left. Seek 1 frame backward
        frame_no -= 1
    elif key == 2621440: # Arrow right. Seek 10 frames forward
        frame_no = max(0, frame_no + 10)
    elif key == 2490368: # Arrow left. Seek 10 frames backward
        frame_no -= 10
    elif key == ord('q'): # Press Q on keyboard to exit
        break

cap.release()
cv2.destroyAllWindows()
