import cv2
import os
from pathlib import Path

directory = 'snapshots'
img_cnt = 0

def snapshot(img, video_file_name):
    global img_cnt
    if not os.path.exists(directory):
        os.makedirs(directory)
    file_basename = Path(video_file_name).stem
    cv2.imwrite('%s/%s_%03d.jpg' % (directory, file_basename, img_cnt), img)
    img_cnt += 1